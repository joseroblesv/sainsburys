package es.technical.test.sainsburys.utils;

public class TextUtils {

	public static boolean isEmptyOrBlank(final String input) {
		return input == null || input.isEmpty();
	}

}
