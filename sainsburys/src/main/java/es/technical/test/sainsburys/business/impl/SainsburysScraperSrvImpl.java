package es.technical.test.sainsburys.business.impl;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.technical.test.sainsburys.business.SainsburysScraperSrv;
import es.technical.test.sainsburys.dao.impl.ProductDao;
import es.technical.test.sainsburys.domain.Product;
import es.technical.test.sainsburys.domain.Response;
import es.technical.test.sainsburys.domain.Total;
import es.technical.test.sainsburys.utils.Constants;
import es.technical.test.sainsburys.utils.TextUtils;

@Service
public class SainsburysScraperSrvImpl implements SainsburysScraperSrv {

	@Autowired
	private ProductDao dao;

	@Override
	public Response getResponse(final String url) throws IOException, IllegalArgumentException {
		if (TextUtils.isEmptyOrBlank(url)) {
			throw new IllegalArgumentException();
		}
		return createResponse(url);
	}

	private Response createResponse(final String url) throws IOException {
		final Response response = new Response();
		addProducts(url, response);
		addTotal(response);
		return response;
	}

	private void addTotal(final Response response) {
		final Total total = new Total();
		total.setGross(dao.getTotalGross());
		total.setVat(dao.getVat());
		response.setTotal(total);
	}

	private void addProducts(final String url, final Response response) throws IOException {
		final Document doc = Jsoup.connect(url).get();
		final Elements productsElements = doc.getElementsByClass(Constants.PRODUCT_SELECTOR);
		for (final Element productElement : productsElements) {
			final Element productUrlElement = productElement.select(Constants.PRODUCT_URL_SELECTOR).first();
			final Product product = createProduct(productUrlElement);
			dao.save(product);
		}
		response.setResults(dao.getAll());
	}

	private Product createProduct(final Element productUrlElement) throws IOException {
		final String urlProduct = productUrlElement.absUrl(Constants.URL_SELECTOR);
		final Document doc = Jsoup.connect(urlProduct).get();
		final Product product = new Product();
		addTitle(product, productUrlElement);
		addPricePerUnit(product, doc);
		addDescription(product, doc);
		addKcal(product, doc);
		return product;
	}

	private void addTitle(final Product product, final Element productUrlElement) {
		product.setTitle(productUrlElement.text());
	}

	private void addKcal(final Product product, final Document doc) {
		final Element kCalElement = doc.select(Constants.TABLE_SELECTOR).select(Constants.KCAL_SELECTOR).first();
		if (kCalElement != null) {
			final Integer kcal = Integer
					.parseInt(kCalElement.text().replaceAll(Constants.REGEX_PATTERN_NO_DIGITS, Constants.BLANK_STRING));
			product.setKcal(kcal);
		}
	}

	private void addDescription(final Product product, final Document doc) {
		final Element description = doc.select(Constants.DESCRIPTION_SELECTOR).first();
		if (description != null) {
			final String desc = description.text();
			product.setDescription(desc);
		}
	}

	private void addPricePerUnit(final Product product, final Document doc) {
		final Element pricePerUnitElement = doc.select(Constants.PRICE_SELECTOR).first();
		final String pricePerUnitString = pricePerUnitElement.text()
				.replaceAll(Constants.REGEX_PATTERN_NO_DIGITS_OR_DOT, Constants.BLANK_STRING);
		final float pricePerUnit = Float.parseFloat(pricePerUnitString);
		product.setUnitPrice(pricePerUnit);
	}

}
