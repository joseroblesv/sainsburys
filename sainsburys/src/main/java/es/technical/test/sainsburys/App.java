package es.technical.test.sainsburys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import es.technical.test.sainsburys.controller.SainsburysScraperController;
import es.technical.test.sainsburys.utils.Constants;

@SpringBootApplication
public class App {

	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		final ApplicationContext applicationContext = SpringApplication.run(App.class, args);
		final SainsburysScraperController sainsburysScraperController = applicationContext
				.getBean(SainsburysScraperController.class);
		sainsburysScraperController.printJsonResult(Constants.URL_HTML_SAINSBURYS);
		SpringApplication.exit(applicationContext);
	}

}
