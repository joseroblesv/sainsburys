package es.technical.test.sainsburys.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "gross", "vat" })
public class Total implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6217048829575485899L;

	@JsonProperty("gross")
	private double gross;

	@JsonProperty("vat")
	private double vat;

	public double getGross() {
		return gross;
	}

	public void setGross(double gross) {
		this.gross = gross;
	}

	public double getVat() {
		return vat;
	}

	public void setVat(double vat) {
		this.vat = vat;
	}

}
