package es.technical.test.sainsburys.utils;

public class Constants {

	public static final String URL_HTML_SAINSBURYS = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk"
			+ "/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";
	public static final String ILLEGAL_ARGUMENT_EXCEPTION = "Please, enter a valid URL. Error: %s";
	public static final String IO_EXCEPTION = "There was a problem while trying to obtain the information. Error: %s";
	public static final String SUCCESS_MESSAGE = "Json obtained successfully: %s";

	public static final String URL_SELECTOR = "href";
	public static final String PRODUCT_URL_SELECTOR = "a[href]";
	public static final String PRODUCT_SELECTOR = "product";
	public static final String KCAL_SELECTOR = "tr.tableRow0 > td";
	public static final String TABLE_SELECTOR = "div.tableWrapper > table";
	public static final String PRICE_SELECTOR = "p.pricePerUnit";
	public static final String DESCRIPTION_SELECTOR = "div.productText > p";
	public static final String BLANK_STRING = "";
	public static final String REGEX_PATTERN_NO_DIGITS_OR_DOT = "[^(.|\\d)]";
	public static final String REGEX_PATTERN_NO_DIGITS = "[^(\\d)]";

	public static final double VAT_PERCENTAGE = 0.20;

}
