package es.technical.test.sainsburys.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "results", "total" })
public class Response implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6052673781857993136L;

	@JsonProperty("results")
	private List<Product> results;

	@JsonProperty("total")
	private Total total;

	public List<Product> getResults() {
		return results;
	}

	public void setResults(List<Product> results) {
		this.results = results;
	}

	public Total getTotal() {
		return total;
	}

	public void setTotal(Total total) {
		this.total = total;
	}

}
