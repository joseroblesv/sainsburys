package es.technical.test.sainsburys.business;

import java.io.IOException;

import es.technical.test.sainsburys.domain.Response;

public interface SainsburysScraperSrv {

	/**
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws IllegalArgumentException
	 */
	public Response getResponse(final String url) throws IOException, IllegalArgumentException;

}
