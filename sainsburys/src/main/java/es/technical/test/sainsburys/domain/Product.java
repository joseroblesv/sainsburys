package es.technical.test.sainsburys.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "title", "kcal_per_100g", "unit_price", "description" })
public class Product implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7356137248666554472L;

	@JsonProperty("title")
	private String title;

	@JsonProperty("kcal_per_100g")
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int kcal;

	@JsonProperty("unit_price")
	private double unitPrice;

	@JsonProperty("description")
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String description;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getKcal() {
		return kcal;
	}

	public void setKcal(int kcal) {
		this.kcal = kcal;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
