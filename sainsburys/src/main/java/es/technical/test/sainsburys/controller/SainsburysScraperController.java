package es.technical.test.sainsburys.controller;

import java.io.IOException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.technical.test.sainsburys.business.SainsburysScraperSrv;
import es.technical.test.sainsburys.domain.Response;
import es.technical.test.sainsburys.utils.Constants;

@Controller
public class SainsburysScraperController {

	final static Logger logger = Logger.getLogger(SainsburysScraperController.class);

	@Autowired
	private SainsburysScraperSrv sainsburysScraperSrv;

	public void printJsonResult(final String url) {
		BasicConfigurator.configure();
		try {
			final Response response = sainsburysScraperSrv.getResponse(Constants.URL_HTML_SAINSBURYS);
			final ObjectMapper mapper = new ObjectMapper();
			final String jsonResponse = mapper.writeValueAsString(response);
			logger.info(String.format(Constants.SUCCESS_MESSAGE, jsonResponse));
		} catch (final IOException e) {
			logger.error(String.format(Constants.IO_EXCEPTION, e.getMessage()));
		} catch (final IllegalArgumentException e) {
			logger.error(String.format(Constants.ILLEGAL_ARGUMENT_EXCEPTION, e.getMessage()));
		}
	}
}
