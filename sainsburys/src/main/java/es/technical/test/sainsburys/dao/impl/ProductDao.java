package es.technical.test.sainsburys.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import es.technical.test.sainsburys.dao.Dao;
import es.technical.test.sainsburys.domain.Product;
import es.technical.test.sainsburys.utils.Constants;

@Component
public class ProductDao implements Dao<Product> {

	private final List<Product> products = new ArrayList<>();

	@Override
	public Optional<Product> get(final int id) {
		return Optional.ofNullable(products.get(id));
	}

	@Override
	public List<Product> getAll() {
		return products;
	}

	@Override
	public void save(final Product product) {
		products.add(product);
	}

	@Override
	public void update(final int id, final Product product) {
		products.set(id, product);
	}

	@Override
	public void delete(final Product product) {
		products.remove(product);
	}

	public Double getTotalGross() {
		return products.stream().mapToDouble(p -> p.getUnitPrice()).sum();
	}

	public Double getVat() {
		return getTotalGross() * Constants.VAT_PERCENTAGE;
	}

}
