package es.technical.test.sainsburys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import es.technical.test.sainsburys.business.SainsburysScraperSrv;
import es.technical.test.sainsburys.dao.impl.ProductDao;
import es.technical.test.sainsburys.domain.Product;
import es.technical.test.sainsburys.domain.Response;
import es.technical.test.sainsburys.utils.Constants;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class SainsburysTest {

	@Autowired
	private SainsburysScraperSrv sainsburysScraperSrv;

	@Autowired
	private ProductDao dao;

	@Test
	public void given_blankString_when_getResponse_then_throwsIllegalArgumentException()
			throws IllegalArgumentException, IOException {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			sainsburysScraperSrv.getResponse(Constants.BLANK_STRING);
		});
	}

	@Test
	public void given_null_when_getResponse_then_throwsIllegalArgumentException()
			throws IllegalArgumentException, IOException {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			sainsburysScraperSrv.getResponse(null);
		});
	}

	@Test
	public void given_badUrl_when_getResponse_then_throwsIOException() throws IllegalArgumentException, IOException {
		final String badUrl = "http://..";
		Assertions.assertThrows(IOException.class, () -> {
			sainsburysScraperSrv.getResponse(badUrl);
		});
	}

	@Test
	public void given_urlSainsburys_when_getResponse_then_notNullResponse()
			throws IllegalArgumentException, IOException {
		final Response response = sainsburysScraperSrv.getResponse(Constants.URL_HTML_SAINSBURYS);
		Assertions.assertTrue(response != null);
	}

	@Test
	public void given_urlSainsburys_when_getResponse_then_productListNotEmpty()
			throws IllegalArgumentException, IOException {
		final Response response = sainsburysScraperSrv.getResponse(Constants.URL_HTML_SAINSBURYS);
		Assertions.assertTrue(response.getResults() != null);
	}

	@Test
	public void given_urlSainsburys_when_getResponse_then_17products() throws IllegalArgumentException, IOException {
		final Response response = sainsburysScraperSrv.getResponse(Constants.URL_HTML_SAINSBURYS);
		final int expectedSize = 17;
		Assertions.assertEquals(expectedSize, response.getResults().size());
	}

	@Test
	public void given_urlSainsburys_when_getResponse_then_firstProductTitleExpected()
			throws IllegalArgumentException, IOException {
		final String firstProductTitleExpected = "Sainsbury's Strawberries 400g";
		final Response response = sainsburysScraperSrv.getResponse(Constants.URL_HTML_SAINSBURYS);
		Assertions.assertEquals(firstProductTitleExpected, response.getResults().get(0).getTitle());
	}

	@Test
	public void given_urlSainsburys_when_getResponse_then_firstProductPriceExpected()
			throws IllegalArgumentException, IOException {
		final double firstProductPriceExpected = 1.75;
		final Response response = sainsburysScraperSrv.getResponse(Constants.URL_HTML_SAINSBURYS);
		Assertions.assertEquals(firstProductPriceExpected, response.getResults().get(0).getUnitPrice());
	}

	@Test
	public void given_urlSainsburys_when_getResponse_then_firstProductDescriptionExpected()
			throws IllegalArgumentException, IOException {
		final String firstProductDescriptionExpected = "by Sainsbury's strawberries";
		final Response response = sainsburysScraperSrv.getResponse(Constants.URL_HTML_SAINSBURYS);
		Assertions.assertEquals(firstProductDescriptionExpected, response.getResults().get(0).getDescription());
	}

	@Test
	public void given_urlSainsburys_when_getResponse_then_firstProductKcalExpected()
			throws IllegalArgumentException, IOException {
		final int firstProductKcalExpected = 33;
		final Response response = sainsburysScraperSrv.getResponse(Constants.URL_HTML_SAINSBURYS);
		Assertions.assertEquals(firstProductKcalExpected, response.getResults().get(0).getKcal());
	}

	@Test
	public void given_urlSainsburys_when_getResponse_then_totalGrossExpected()
			throws IllegalArgumentException, IOException {
		final double totalGrossExpected = 39.50;
		final Response response = sainsburysScraperSrv.getResponse(Constants.URL_HTML_SAINSBURYS);
		Assertions.assertEquals(totalGrossExpected, response.getTotal().getGross());
	}

	@Test
	public void given_urlSainsburys_when_getResponse_then_totalVatExpected()
			throws IllegalArgumentException, IOException {
		final double totalVatExpected = 7.90;
		final Response response = sainsburysScraperSrv.getResponse(Constants.URL_HTML_SAINSBURYS);
		Assertions.assertEquals(totalVatExpected, response.getTotal().getVat());
	}

	@Test
	public void given_dao_when_save_then_productSavedCorrectly() throws IOException {
		final Product p1 = new Product();
		p1.setUnitPrice(2);
		p1.setKcal(33);
		p1.setTitle("Sainsbury's Strawberries 400g");
		p1.setDescription("by Sainsbury's strawberries");
		dao.save(p1);
		final Optional<Product> optional = dao.get(0);
		optional.ifPresent(product -> {
			Assertions.assertEquals(p1, product);
		});
	}

	@Test
	public void given_dao_when_getAll_then_getAllProducts() throws IOException {
		final Product p1 = new Product();
		p1.setUnitPrice(2);
		p1.setKcal(33);
		p1.setTitle("Sainsbury's Strawberries 400g");
		p1.setDescription("by Sainsbury's strawberries");
		final List<Product> products = new ArrayList<Product>();
		products.add(p1);
		dao.save(p1);
		Assertions.assertEquals(products, dao.getAll());
	}

	@Test
	public void given_dao_when_delete_then_productDeletedCorrectly() throws IOException {
		final Product p1 = new Product();
		p1.setUnitPrice(2);
		p1.setKcal(33);
		p1.setTitle("Sainsbury's Strawberries 400g");
		p1.setDescription("by Sainsbury's strawberries");
		final List<Product> products = new ArrayList<Product>();
		products.add(p1);
		dao.save(p1);
		dao.delete(p1);
		Assertions.assertTrue(dao.getAll().isEmpty());
	}

	@Test
	public void given_dao_when_update_then_productUpdatedCorrectly() throws IOException {
		final Product p1 = new Product();
		p1.setUnitPrice(2);
		p1.setKcal(33);
		p1.setTitle("Sainsbury's Strawberries 400g");
		p1.setDescription("by Sainsbury's strawberries");
		final List<Product> products = new ArrayList<Product>();
		products.add(p1);
		dao.save(p1);
		final Product p2 = new Product();
		p1.setUnitPrice(3);
		p1.setKcal(35);
		p1.setTitle("Sainsbury's Raspberries 400g");
		p1.setDescription("by Sainsbury's raspberries");
		dao.update(0, p2);
		final Optional<Product> optional = dao.get(0);
		optional.ifPresent(product -> {
			Assertions.assertEquals(p2, product);
		});
	}

}
